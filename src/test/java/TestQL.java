import com.ql.util.express.DefaultContext;
import com.ql.util.express.ExpressRunner;
import com.ql.util.express.Operator;
import com.ql.util.express.instruction.op.OperatorAnd;
import com.ql.util.express.instruction.op.OperatorAnonymousNewList;
import com.ql.util.express.instruction.op.OperatorOr;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestQL {

    private static final Logger logger = Logger.getLogger("a");

    @Test
    public void Test1() throws Exception {
        logger.info(11);
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        context.put("a", 1);
        context.put("b", 2);
        context.put("c", 3);
        String express = "a+b*c";
        Object r = runner.execute(express, context, null, true, false);
        logger.info(r);
        r = runner.execute(express, context, null, true, false);
        logger.info(r);
    }

    @Test
    public void Test2() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        runner.addOperatorWithAlias("如果", "if", null);
        runner.addOperatorWithAlias("则", "then", null);
        runner.addOperatorWithAlias("否则", "else", null);

        String exp = "如果  (语文+数学+英语>270) 则 {return 1;} 否则 {return 0;}";
        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        context.put("语文", 10);
        context.put("数学", 310);
        context.put("英语", 10);
        Object r = runner.execute(exp, context, null, false, false, null);
        logger.info(r);

        context.put("语文", 10);
        context.put("数学", 10);
        context.put("英语", 10);
        r = runner.execute(exp, context, null, false, false, null);
        logger.info(r);
    }

    @Test
    public void Test3() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        runner.addOperator("包含", new ContainsOperator());
        runner.addOperator("不包含", new NotContainsOperator());
        runner.addOperatorWithAlias("如果", "if", null);
        runner.addOperatorWithAlias("则", "then", null);
        runner.addOperatorWithAlias("否则", "else", null);
        runner.addOperatorWithAlias("返回", "return", null);

        // 系统功能重命名
        runner.addOperator("且", new OperatorAnd("&&"));
        runner.addOperator("或", new OperatorOr("||"));
        runner.getOperatorFactory().addOperator("列表", new OperatorAnonymousNewList("NewList"));

        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        // 这里是变量
        context.put("$标签组", Arrays.asList("本人", "不大1"));
        StringBuilder express = new StringBuilder();
        express.append(" 如果 ( 列表('亲戚','父母','弟弟') 包含 $标签组 ) 且 ( 列表('很大','很老') 包含 $标签组 ) 则 {返回 '非本人';} ");
        express.append(" 否则 如果 ( 列表('我','本人') 包含 $标签组 ) 且 ( 列表('不大') 包含 $标签组 ) 则 {返回 '本人';}  ");
        express.append(" 否则 如果 ( 列表('我','本人') 包含 $标签组 ) 且 ( 列表('很好') 不包含 $标签组 ) 则 {返回 '本人2';}  ");
        express.append(" 否则 {返回 '其他';} ");
//        String express = "如果 ( 列表('亲戚','父母','弟弟') 包含 $标签组 ) 则 {返回 '非本人';} " +
//                " 否则 如果 ( 列表('我','本人') 包含 $标签组 ) {返回 '本人';} " +
//                " 否则 {返回 '其他';} ";
        logger.info("表达式：\t" + express);
        Object r = runner.execute(express.toString(), context, null, true, false);
        logger.info("结果：\t" + r);
    }

    //定义一个继承自com.ql.util.express.Operator的操作符
    public static class ContainsOperator extends Operator {
        public Object executeInner(Object[] list) throws Exception {
            Object opdata1 = list[0];
            Object opdata2 = list[1];
            if (opdata1 instanceof java.util.List
                    && opdata2 instanceof java.util.List) {
                List<String> list1 = (List) opdata1;
                List<String> list2 = (List) opdata2;
                logger.debug("包含 参数： key:" + list2 + "\tlist:" + list1);
                for (String tag : list2) {
                    if (list1.contains(tag)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    //定义一个继承自com.ql.util.express.Operator的操作符
    public static class NotContainsOperator extends Operator {
        public Object executeInner(Object[] list) throws Exception {
            Object opdata1 = list[0];
            Object opdata2 = list[1];
            if (opdata1 instanceof java.util.List
                    && opdata2 instanceof java.util.List) {
                List<String> list1 = (List) opdata1;
                List<String> list2 = (List) opdata2;
                logger.debug("包含 参数： key:" + list2 + "\tlist:" + list1);
                for (String tag : list2) {
                    if (list1.contains(tag)) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }


    @Test
    public void Test4() {
        StringBuilder express = new StringBuilder();
        express.append(" \n ");
        express.append(" 如果 ( 列表('亲戚','父母','弟弟') 包含 $标签组 ) 且 ( 列表('很大','很老') 包含 $标签组 ) 则 {返回 '非本人';} \n");
        express.append(" 否则 如果 ( 列表('我','本人') 包含 $标签组 ) 且 ( 列表('不大') 包含 $标签组 ) 则 {返回 '本人';}  \n");
        express.append(" 否则 如果 ( 列表('我','本人') 包含 $标签组 ) 且 ( 列表('很好') 不包含 $标签组 ) 则 {返回 '本人2';}  \n");
        express.append(" 否则 {返回 '其他';} \n ");

        try {
            ExpressRunner runner = new ExpressRunner();
            runner.addOperator("包含", new ContainsOperator());
            runner.addOperator("不包含", new NotContainsOperator());
            runner.addOperatorWithAlias("如果", "if", null);
            runner.addOperatorWithAlias("则", "then", null);
            runner.addOperatorWithAlias("否则", "else", null);
            runner.addOperatorWithAlias("返回", "return", null);

            // 系统功能重命名
            runner.addOperator("且", new OperatorAnd("&&"));
            runner.addOperator("或", new OperatorOr("||"));
            runner.getOperatorFactory().addOperator("列表", new OperatorAnonymousNewList("NewList"));

//        String express = "如果 ( 列表('亲戚','父母','弟弟') 包含 $标签组 ) 则 {返回 '非本人';} " +
//                " 否则 如果 ( 列表('我','本人') 包含 $标签组 ) {返回 '本人';} " +
//                " 否则 {返回 '其他';} ";
            logger.info("表达式：\t" + express);
            String[] names = runner.getOutVarNames(express.toString());
            logger.info("结果：\t" + Arrays.asList(names));
        } catch (Exception e) {
            logger.error("异常：\t" + express.toString(), e);
        }
    }

    @Test
    public void Test5() throws Exception {
        logger.info("start...");
        StringBuilder express = new StringBuilder();
        express.append(" \n ");
        express.append(" 如果 ( 列表('亲戚','父母','弟弟') 包含 $标签组 ) 且 ( 列表('很大','很老') 包含 $标签组 ) 则 {返回 '非本人';} \n");
        express.append(" 否则 如果 ( 列表('我','本人') 包含 $标签组 ) 且 ( 列表('不大') 包含 $标签组 ) 则 {返回 '本人';}  \n");
        express.append(" 否则 如果 ( 列表('我','本人') 包含 $标签组 ) 且 ( 列表('很好') 不包含 $标签组 ) 则 {返回 '本人2';}  \n");
        express.append(" 否则 {返回 '其他';} \n ");

        String res = getExpressRunner(express.toString(), Arrays.asList("本人", "不大1"));
        logger.info("表达式：\t" + express);
        logger.info("结果：\t" + res);
    }

    public static String getExpressRunner(String express, List<String> tags) throws Exception {
        ExpressRunner runner = new ExpressRunner();
        runner.addOperator("包含", new ContainsOperator());
        runner.addOperator("不包含", new NotContainsOperator());
        runner.addOperatorWithAlias("如果", "if", null);
        runner.addOperatorWithAlias("则", "then", null);
        runner.addOperatorWithAlias("否则", "else", null);
        runner.addOperatorWithAlias("返回", "return", null);

        // 系统功能重命名
        runner.addOperator("且", new OperatorAnd("&&"));
        runner.addOperator("或", new OperatorOr("||"));
        runner.getOperatorFactory().addOperator("列表", new OperatorAnonymousNewList("NewList"));

        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        // 这里是变量
        context.put("$标签组", tags);
        Object r = runner.execute(express, context, null, true, false);
//        logger.info("结果：\t" + r);
//        r = runner.execute(express, context, null, true, false);
//        logger.info("结果：\t" + r);
//        r = runner.execute(express, context, null, true, false);
//        logger.info("结果：\t" + r);

        return (String) r;
    }

}
