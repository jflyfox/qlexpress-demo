import com.ql.util.express.DefaultContext;
import com.ql.util.express.ExpressRunner;
import com.ql.util.express.Operator;
import org.junit.Test;

public class TestQLDemo {

    @Test
    public void testSet() throws Exception {
        ExpressRunner runner = new ExpressRunner(false,false);
        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        String express = "abc = NewMap(1:1,2:2); return abc.get(1) + abc.get(2);";
        Object r = runner.execute(express, context, null, false, false);
        System.out.println(r);
        express = "abc = NewList(1,2,3); return abc.get(1)+abc.get(2)";
        r = runner.execute(express, context, null, false, false);
        System.out.println(r);
        express = "abc = [1,2,3]; return abc[1]+abc[2];";
        r = runner.execute(express, context, null, false, false);
        System.out.println(r);
    }
    /**
     //(1)addOperator
     ExpressRunner runner = new ExpressRunner();
     DefaultContext<String, Object> context = new DefaultContext<String, Object>();
     runner.addOperator("join",new JoinOperator());
     Object r = runner.execute("1 join 2 join 3", context, null, false, false);
     System.out.println(r);
     //返回结果  [1, 2, 3]

     //(2)replaceOperator
     ExpressRunner runner = new ExpressRunner();
     DefaultContext<String, Object> context = new DefaultContext<String, Object>();
     runner.replaceOperator("+",new JoinOperator());
     Object r = runner.execute("1 + 2 + 3", context, null, false, false);
     System.out.println(r);
     //返回结果  [1, 2, 3]

     //(3)addFunction
     ExpressRunner runner = new ExpressRunner();
     DefaultContext<String, Object> context = new DefaultContext<String, Object>();
     runner.addFunction("join",new JoinOperator());
     Object r = runner.execute("join(1,2,3)", context, null, false, false);
     System.out.println(r);
     //返回结果  [1, 2, 3]
     */
}

//定义一个继承自com.ql.util.express.Operator的操作符
class JoinOperator extends Operator {
    public Object executeInner(Object[] list) throws Exception {
        Object opdata1 = list[0];
        Object opdata2 = list[1];
        if(opdata1 instanceof java.util.List){
            ((java.util.List)opdata1).add(opdata2);
            return opdata1;
        }else{
            java.util.List result = new java.util.ArrayList();
            result.add(opdata1);
            result.add(opdata2);
            return result;
        }
    }
}